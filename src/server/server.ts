import { ApolloServer } from "apollo-server-fastify";
import { buildFederatedSchema } from "@apollo/federation";
import { config } from "../utils/config";
import { context } from "./context";
import { typeDefs } from "./type-defs";
import { resolvers } from "./resolvers";

export const server = new ApolloServer({
  schema: buildFederatedSchema([{ typeDefs, resolvers }]),
  context,
  playground: config.playground
});
