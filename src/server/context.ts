import { config } from "../utils/config";

export type UserInfo = {
  token: string;
  roles: string[];
  payload?: { [key: string]: any };
};

export type RawToken = {
  jwt: boolean;
  value: string;
};

const emptyUserInfo = {
  token: "",
  roles: []
};

export const context = ({ req }: { req: any }): UserInfo => {
  const authorizationHeader = String(req.headers.authorization) || "";
  const rawToken = extractTokenFromAuthorizationHeader(authorizationHeader);
  if (!rawToken.value) {
    return emptyUserInfo;
  }
  return computeToken(rawToken);
};

/**
 * First case is a simple Bearer token
 * Second case is a JWT
 */

const extractTokenFromAuthorizationHeader = (
  authorizationHeader: string
): RawToken =>
  authorizationHeader.startsWith("Bearer ")
    ? { jwt: false, value: authorizationHeader.split(" ")[1] }
    : { jwt: false, value: authorizationHeader };

const computeToken = (rawToken: RawToken): UserInfo => {
  // Not implemented yet
  if (rawToken.jwt) {
    return {
      token: rawToken.value,
      roles: []
    };
  } else {
    const roles = authenticateUsingBearerToken(rawToken.value) ? ["admin"] : [];
    return {
      token: rawToken.value,
      roles
    };
  }
};

const authenticateUsingBearerToken = (token: string) =>
  config.admin_token === token;
