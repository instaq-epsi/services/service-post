import { readFileSync } from "fs";
import { config } from "../utils/config";
import { gql } from "apollo-server-core";
import { DocumentNode } from "graphql";

const parseTypeDefs = (): DocumentNode => {
  return gql(readFileSync(config.schema_path).toString());
};

export const typeDefs = parseTypeDefs();
