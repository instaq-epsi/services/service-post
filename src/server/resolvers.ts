import { prisma, PostCreateInput } from "../prisma-client";

export const resolvers: any = {
  Query: {
    posts: async (parent: any, args: any, context: any, info: any) => {
      return prisma.posts(args);
    }
  },
  Mutation: {
    createUser: (
      _: any,
      { data }: { data: PostCreateInput },
      { dataSources }: any
    ) => {
      const result = prisma.createPost(data);
      result.catch(err => console.log(err));
      return result;
    }
  }
};
