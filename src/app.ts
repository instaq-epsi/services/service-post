import fastify from "fastify";
import { config } from "./utils/config";
import { server } from "./server/server";

const app = fastify();

(async () => {
  app.register(server.createHandler());
  const address = await app.listen(config.port, config.host);
  console.log(`${config.service}-service started on ${address}`);
})();
